<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221229125715 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE movie_actor (movie_id INT NOT NULL, actor_id INT NOT NULL, INDEX IDX_3A374C658F93B6FC (movie_id), INDEX IDX_3A374C6510DAF24A (actor_id), PRIMARY KEY(movie_id, actor_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie_director (movie_id INT NOT NULL, director_id INT NOT NULL, INDEX IDX_C266487D8F93B6FC (movie_id), INDEX IDX_C266487D899FB366 (director_id), PRIMARY KEY(movie_id, director_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE movie_actor ADD CONSTRAINT FK_3A374C658F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_actor ADD CONSTRAINT FK_3A374C6510DAF24A FOREIGN KEY (actor_id) REFERENCES actor (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_director ADD CONSTRAINT FK_C266487D8F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_director ADD CONSTRAINT FK_C266487D899FB366 FOREIGN KEY (director_id) REFERENCES director (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE actor ADD first_name VARCHAR(255) DEFAULT NULL, ADD last_name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE composer ADD first_name VARCHAR(255) DEFAULT NULL, ADD last_name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE director ADD first_name VARCHAR(255) DEFAULT NULL, ADD last_name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE movie ADD composer_id INT DEFAULT NULL, ADD release_date DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', ADD synopsis LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE movie ADD CONSTRAINT FK_1D5EF26F7A8D2620 FOREIGN KEY (composer_id) REFERENCES composer (id)');
        $this->addSql('CREATE INDEX IDX_1D5EF26F7A8D2620 ON movie (composer_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE movie_actor DROP FOREIGN KEY FK_3A374C658F93B6FC');
        $this->addSql('ALTER TABLE movie_actor DROP FOREIGN KEY FK_3A374C6510DAF24A');
        $this->addSql('ALTER TABLE movie_director DROP FOREIGN KEY FK_C266487D8F93B6FC');
        $this->addSql('ALTER TABLE movie_director DROP FOREIGN KEY FK_C266487D899FB366');
        $this->addSql('DROP TABLE movie_actor');
        $this->addSql('DROP TABLE movie_director');
        $this->addSql('ALTER TABLE actor DROP first_name, DROP last_name');
        $this->addSql('ALTER TABLE composer DROP first_name, DROP last_name');
        $this->addSql('ALTER TABLE director DROP first_name, DROP last_name');
        $this->addSql('ALTER TABLE movie DROP FOREIGN KEY FK_1D5EF26F7A8D2620');
        $this->addSql('DROP INDEX IDX_1D5EF26F7A8D2620 ON movie');
        $this->addSql('ALTER TABLE movie DROP composer_id, DROP release_date, DROP synopsis');
    }
}
